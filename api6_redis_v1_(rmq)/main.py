"""
Микросервис для хранения кешированных данных

Через RabbitMQ получает и обрабатывает следующие типы запросов:
1) {'type': 'find', 'country': 'Название_страны'}
это запрос на поиск значения в кэше
результат возвращает в эту же очередь в формате:
{'type': 'answer', 
 'result': 0 (если данные не найдены) или 1 (если данные в кэше найдены), 
 'value': Кешированное значение}

2) {'type': 'save', 'country': 'Название_страны (ключ)', 'url': 'Ссылка на веб-сайт (значение)'}
это запрос на сохранение в кеше переданного ключа и значения
Ответ не возвращает

Настройки:
переменные окружения
RMQ_(HOST/PORT/USER/PASSWORD) - параметры соединения с сервисом RMQ
RMQ_EXCH_CACHE - канал для получения запросов и отправки ответов
REDIS_(HOST/PORT) - параметры соединения с сервисом Redis
"""

import pika
import redis
import os
import json


redis_host = os.environ.get("REDIS_HOST")
redis_port = os.environ.get("REDIS_PORT")

cache = redis.Redis(host=redis_host, port=redis_port)

user = os.environ.get("RMQ_USER")
password = os.environ.get("RMQ_PASSWORD")
host = os.environ.get("RMQ_HOST")
port = os.environ.get("RMQ_PORT")
exch_name = os.environ.get("RMQ_EXCH_CACHE")

def cb(ch, method, properties, body):
    # print(body)
    in_params = json.loads(body)
    if in_params['type'] == 'find':
        # поиск значения в кеше
        cache_answer = cache.get(in_params['country']).decode('utf-8')
        if cache_answer is None:
            params = {'type': 'answer', 'result': 0}
        else:
            params = {'type': 'answer', 'result': 1, 'value': cache_answer}
        # отправляю ответ в тот же канал, откуда получил запрос
        ch.basic_publish(exchange=exch_name, routing_key="", body=f"{json.dumps(params)}")
    elif in_params['type'] == 'save':
        # сохранить новое значение в кэш
        cache.mset({in_params['country']: in_params['url']})
    else:
        # прочие типы (например 'answer') не обрабатываем
        pass

print(f'connecting to RMQ: [{host}:{port}]')

parameters = pika.URLParameters(f"amqp://{user}:{password}@{host}:{port}")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.exchange_declare(exchange=exch_name, exchange_type="fanout")

result = channel.queue_declare(queue="", exclusive=False)
queue_name = result.method.queue
channel.queue_bind(exchange=exch_name, queue=queue_name)

print(f'channel.basic_consume')
channel.basic_consume(queue=queue_name, on_message_callback=cb, auto_ack=True)

print("waiting for cache requests")
channel.start_consuming()
