"""
Сервис базы данных, хранение информации о запросах
Используется простая MySQL база

❆ Добавление новой записи:
POST-запрос на адрес /save
Параметры запроса (json):
{'country': 'Название страны', 'email': 'Адрес электронной почты, на которую отправить результат'}

"""

from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func, UniqueConstraint
import logging
import json

from datetime import datetime

# import sentry_sdk
# from sentry_sdk.integrations.flask import FlaskIntegration

# sentry_sdk.init(
#     dsn="https://23e852b1b1a24965bd26a9b73459b255@o1095196.ingest.sentry.io/6130526",
#     integrations=[FlaskIntegration()],

#     # Set traces_sample_rate to 1.0 to capture 100%
#     # of transactions for performance monitoring.
#     # We recommend adjusting this value in production.
#     traces_sample_rate=1.0
# )

app = Flask(__name__)

app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///hw22_2.db"
db = SQLAlchemy(app)

debug_logger = logging.getLogger("debug_logger")
debug_logger.setLevel(logging.DEBUG)
fh = logging.FileHandler("api2.log" ,"w", "utf-8")
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
fh.setFormatter(formatter)
debug_logger.addHandler(fh)

# ------------------------------------------------------------------------------------
class RequestsLog(db.Model):
    __tablename__ = "reqlog"
    id = db.Column(db.Integer, primary_key=True)
    country = db.Column(db.String(50))
    email = db.Column(db.String(100))
    dttm = db.Column(db.DateTime)

    def __init__(self, country_name, email) -> None:
        self.country = country_name
        self.email = email
        self.dttm = datetime.now()

    def __str__(self) -> str:
        return f"{self.id}"

class Subscriber(db.Model):
    __tablename__ = "rep_subscribe"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    email = db.Column(db.String(100))
    __table_args__ = (
        UniqueConstraint('email', name='unique_emails'),
        {}
        )

    def __init__(self, subscriber_name, subscriber_email) -> None:
        self.name = subscriber_name
        self.email = subscriber_email

    def __str__(self) -> str:
        return f"{self.id}"


db.create_all()

# ------------------------------------------------------------------------------------
@app.route("/")
def home():
    # cnt = RequestsLog.query().count()
    try:
        # count_q = db.session.query(RequestsLog).statement.with_only_columns([func.count()]).order_by(None)
        count_q = RequestsLog.query.statement.with_only_columns([func.count()]).order_by(None)
        cnt = db.session.execute(count_q).scalar()
    except:
        cnt = '?'
    return f"<h2>mbutorin DB</h2><br/><br/>num of records: <b>{cnt}</b>"

# ------------------------------------------------------------------------------------
@app.route("/save", methods=["POST"])
def add():
    data1 = request.data
    debug_logger.debug(f'add(): request.data={request.data}')
    data = json.loads(request.data.decode('utf-8'))
    debug_logger.debug(f'add(): data["country"]={data["country"]}, data["email"]={data["email"]}')
    new_request = RequestsLog(country_name=data["country"], email=data["email"])
    db.session.add(new_request)
    db.session.commit()
    data['datetime'] = new_request.dttm
    return jsonify(data)

# ------------------------------------------------------------------------------------
@app.route("/records", methods=["GET"])
def list_records():
    """Получение списка записей
    GET-запрос на адрес /records
    
    Ответ (json):
    {[{'country': 'Название страны', 'email': 'Адрес электронной почты', 'dttm': 'Дата и время запроса'}], [{...}], }
    """

    result = []
    for i in RequestsLog.query.all():
        result.append({'id': i.id, 'country': i.country, 'email': i.email, 'dttm': i.dttm})
    return jsonify(result)

# ------------------------------------------------------------------------------------
@app.route("/subscribe", methods=["POST"])
def add_subscriber():
    """Добавление подписчика на получение отчёта
    POST-запрос на адрес /subscribe
    Параметры запроса (json):
    {'name': 'Имя подписчика', 'email': 'Адрес электронной почты'}
    """
    try:
        debug_logger.debug(f'add_subscriber(): request.data={request.data}')
        data = json.loads(request.data.decode('utf-8'))
        new_subscr = Subscriber(subscriber_name=data["name"], subscriber_email=data["email"].lower())
        db.session.add(new_subscr)
        db.session.commit()
        data['result'] = 'ok'
    except Exception as ex:
        data['result'] = 'ERROR'
        data['error_text'] = ex.args[0]
    return jsonify(data)

# ------------------------------------------------------------------------------------
@app.route("/subscribers", methods=["GET"])
def list_subscribers():
    """Получение списка подписчиков
    GET-запрос на адрес /subscribers
    
    Ответ (json):
    {[{'name': 'Имя подписчика', 'email': 'Адрес электронной почты'}], [{...}], }
    """

    result = []
    for i in Subscriber.query.all():
        result.append({'id': i.id, 'name': i.name, 'email': i.email})
    return jsonify(result)

# ------------------------------------------------------------------------------------
if __name__ == "__main__":
    app.run()
