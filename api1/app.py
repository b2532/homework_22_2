from flask import Flask, flash, render_template, request, redirect, url_for, jsonify
import requests
import os
import logging
import json

from rmq_anthem import send_RMQ_get_anthem
from rmq_url import send_RMQ_get_country_URL

# import sentry_sdk
# from sentry_sdk.integrations.flask import FlaskIntegration

# sentry_sdk.init(
#     dsn="https://23e852b1b1a24965bd26a9b73459b255@o1095196.ingest.sentry.io/6130526",
#     integrations=[FlaskIntegration()],

#     # Set traces_sample_rate to 1.0 to capture 100%
#     # of transactions for performance monitoring.
#     # We recommend adjusting this value in production.
#     traces_sample_rate=1.0
# )

app = Flask(__name__, static_url_path='/static')
# app.config.from_object("config")
app.secret_key = os.environ.get("SECRET_KEY")
# app.secret_key = app.config["SECRET_KEY"]

debug_logger = logging.getLogger("debug_logger")
debug_logger.setLevel(logging.DEBUG)
fh = logging.FileHandler("api1.log" ,"w", "utf-8")
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
fh.setFormatter(formatter)
debug_logger.addHandler(fh)

@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == "GET":
        return render_template("index.html")
    elif request.method == "POST":
        debug_logger.debug(f'POST ------------')

        data = {}
        data["email"] = request.form["email"]
        data["country"] = request.form["country"]
        debug_logger.debug(f'POST, data=[{data}]')

        # сохраняем данные запроса в базу данных
        host_db = os.environ.get("API_DB")
        resp = requests.post(host_db+"/save", data=json.dumps(data))

        # ищем URL в кэше
        host_cache = os.environ.get("API_CACHE")
        params = {'country': request.form["country"]}
        debug_logger.debug(f'POST, params for cash=[{params}]')
        resp = requests.get(host_cache+"/find", data=json.dumps(params))
        # resp = requests.get(host_cache+"/find", params=params)
        cache_answer = resp.json()
        debug_logger.debug(f'POST, cache answer=[{cache_answer}]')
                
        if cache_answer['result'] == '1':
            flash(f"We have cached value: {cache_answer['url']}")
            send_RMQ_get_anthem(country=data["country"], country_url=cache_answer["url"], email=data["email"])
        else:
            send_RMQ_get_country_URL(country=data["country"], email=data["email"])

        flash(f"Anthem of {data['country']} will be sent to {data['email']}")
        return redirect(url_for("index"))

if __name__ == "__main__":
    app.run()
