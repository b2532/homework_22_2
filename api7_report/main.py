"""
Микросервис для создания отчёта и отправки его на почту подписчикам

Настройки:
Подписчики хранятся в базе данных

переменные окружения
REPORT_INTERVAL - Интервал выдачи отчета
EMAIL_ADDR, EMAIL_PSW, SMTP_SERVER_ADDR, SMTP_SERVER_PORT - Параметры отправителя
"""

import time
import os
import logging
import requests
from report import make_PDF_report, register_PDF_font

from butorin_sendmail import send_email

# ---------------------------------------------------------------------------------
debug_logger = logging.getLogger("debug_logger")
debug_logger.setLevel(logging.DEBUG)
fh = logging.FileHandler("api7.log" ,"w", "utf-8")
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
fh.setFormatter(formatter)
debug_logger.addHandler(fh)

# ---------------------------------------------------------------------------------
# считываем настройки
rep_interval = int(os.environ.get("REPORT_INTERVAL"))

# ---------------------------------------------------------------------------------
register_PDF_font()

# ---------------------------------------------------------------------------------
# бесконечный цикл отправки
while True:
    time.sleep(rep_interval)
    host_db = os.environ.get("API_DB")
    debug_logger.debug('Start report')
    
    # список получателей
    resp = requests.get(host_db+"/subscribers")
    subscribers = resp.json()
    debug_logger.debug(f'subscribers={subscribers}')

    if len(subscribers) == 0:
        continue

    # записи для отчёта
    resp = requests.get(host_db+"/records")
    records = resp.json()
    debug_logger.debug(f'records={records}')
    
    # формирование отчёта
    debug_logger.debug('Make report')
    dir_temp = os.path.join(os.getcwd(), 'temp')
    if not os.path.isdir(dir_temp):
        os.mkdir(dir_temp)
    file_report = os.path.join(dir_temp, f'Report_MVB_HW_22-2.pdf')

    make_PDF_report(file_report, records)

    # отправка
    for receiver in subscribers:
        debug_logger.debug(f'send to: {receiver["email"]}')
        send_email(
            email_to=receiver['email'], 
            subj='Отчёт о заказанных гимнах',
            body=f'Добрый день, {receiver["name"]}\n\nОтчёт о запросах на получение гимнов во вложении',
            # body=debug_body,
            attaches=[file_report,]
            )
    
    os.remove(file_report)
