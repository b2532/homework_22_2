import json
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Paragraph
from reportlab.lib.units import cm
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import ParagraphStyle

# ---------------------------------------------------------------------------------
def make_test_report(filename: str, records: json):
    """Заглушка для отладки, формирование отчета в виде текстового файла

    Args:
        filename (str): имя файла
        records (json): массив значений для отчёта
    """

    filename_txt = filename + '.txt'
    with open(filename_txt, 'w', encoding='utf-8') as f:
        for r in records:
            f.write(f'{r["dttm"]} : {r["country"]} >> {r["email"]}\n')

# ---------------------------------------------------------------------------------
def register_PDF_font():
    from reportlab.pdfbase import pdfmetrics
    from reportlab.pdfbase.ttfonts import TTFont
    
    pdfmetrics.registerFont(TTFont('DejaVuSerif', 'DejaVuSerif.ttf'))

# ---------------------------------------------------------------------------------
def make_PDF_report(filename: str, records: json):
    """Формирование отчета в формате PDF

    Args:
        filename (str): имя файла
        records (json): массив значений для отчёта
    """

    doc = SimpleDocTemplate(filename, pagesize=A4)

    # container for the 'Flowable' objects
    elements = []

    p = Paragraph(
        "<b>Отчёт о запросах на скачивание гимна</b>", 
        style = ParagraphStyle(
            name='Normal',
            fontName='DejaVuSerif',
            fontSize=16,
            spaceAfter=10,
            )
        )
    elements.append(p)

    p = Paragraph(
        "<b><i>(Буторин М.В., домашняя работа #22.2)</i></b>", 
        style = ParagraphStyle(
            name='Normal',
            fontName='DejaVuSerif',
            fontSize=12,
            spaceAfter=15,
            )
        )
    elements.append(p)

    data = [['Дата и время запроса','Страна','Адрес электронной почты'],]
    for r in records:
        data.append([r["dttm"], r["country"], r["email"]])

    t = Table(data) # , 5*[0.4*inch], 4*[0.4*inch]
    t.setStyle(TableStyle(
        [
            ('FONTNAME',(0,0),(-1,-1),'DejaVuSerif'),
            ('TEXTCOLOR',(0,0),(2,0),colors.white),
            ('BACKGROUND',(0,0),(2,0),colors.navy),
            ('ALIGN',(0,0),(-1,-1),'LEFT'),
            ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
            ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
            ('BOX', (0,0), (-1,-1), 0.25, colors.black)
        ]
        ))

    elements.append(t)

    doc.build(elements)

