""" Отправка сообщения электронной почты
Модуль содержит единственную функцию

Для работы необходимо в переменных среды операционной системы прописать 4 переменные, параметры для отправки:
EMAIL_ADDR - адрес отправителя
EMAIL_PSW - пароль отправителя
SMTP_SERVER_ADDR - адрес сервера 
SMTP_SERVER_PORT - порт почтового сервера (SSL)

"""

import os
import smtplib
from email.message import EmailMessage

def send_email(email_to:str, subj:str, body:str, attaches:list=None)->bool:
    """ Отправка почтового сообщения на заданный адрес
    Значения отправителя (почтовый ящик, пароль) и почтового сервера отправителя (адрес, порт) -
    берутся из переменных окружения операционной системы

    Args:
        email_to (str): Адрес почты получателя
        subj (str): Тема письма
        body (str): Содержание письма
        attaches (list): Список имён файлов  (attach as binary data)

    Returns:
        bool: Результат отправки (True - отправлено, False - любые ошибки при отправке)
    """

    try:
        # зачитываем параметры для отправки из переменных среды
        EMAIL_FROM_ADDR = os.environ.get('EMAIL_ADDR')
        EMAIL_FROM_PSWD = os.environ.get('EMAIL_PSW')
        EMAIL_SMTP_SERVER_ADDR = os.environ.get('SMTP_SERVER_ADDR')
        EMAIL_SMTP_SERVER_PORT = os.environ.get('SMTP_SERVER_PORT')

        msg = EmailMessage()
        msg['Subject'] = subj
        msg['From'] = EMAIL_FROM_ADDR
        msg['To'] = email_to
        # msg['Cc'] = 'mvb.nnov@mail.ru' #debug
        msg.set_content(body)
        # raise ValueError(f'sending, from={EMAIL_FROM_ADDR}, to={email_to}, body=[{body}]')
        if attaches != None:
            for filename in attaches:
                if os.path.exists(filename):
                    with open(filename, "rb") as f:
                        file_data = f.read()
                    msg.add_attachment(file_data, maintype = 'bytes', subtype = 'base64', filename = os.path.basename(filename))
                    # msg.add_attachment(file_data, filename = os.path.basename(filename))

        # print('email: ', EMAIL_FROM_ADDR, EMAIL_FROM_PSWD, EMAIL_SMTP_SERVER_ADDR, EMAIL_SMTP_SERVER_PORT)
        # print(msg)

        # with smtplib.SMTP_SSL() as smtp:
        #     smtp.connect(EMAIL_SMTP_SERVER_ADDR, EMAIL_SMTP_SERVER_PORT)
        with smtplib.SMTP_SSL(EMAIL_SMTP_SERVER_ADDR, EMAIL_SMTP_SERVER_PORT) as smtp:
            # smtp.ehlo()
            # smtp.starttls()
            # smtp.ehlo()
            
            smtp.login(EMAIL_FROM_ADDR, EMAIL_FROM_PSWD)
            smtp.send_message(msg)
        
        # raise(f'send OK: EMAIL_FROM_ADDR={EMAIL_FROM_ADDR}, EMAIL_SMTP_SERVER_ADDR={EMAIL_SMTP_SERVER_ADDR}')
        return True
    except Exception as ex:
        print('Send Mail Exception: ', ex.args[0])
        # raise(f'send ERR: EMAIL_FROM_ADDR={EMAIL_FROM_ADDR}, EMAIL_SMTP_SERVER_ADDR={EMAIL_SMTP_SERVER_ADDR}, [{ex.args[0]}]')
        raise
        return False
