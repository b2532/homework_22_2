certifi==2021.10.8
charset-normalizer==2.0.10
idna==3.3
Pillow==9.0.0
pytest==6.2.5
reportlab==3.6.5
requests==2.27.1
urllib3==1.26.8
