import requests
from bs4 import BeautifulSoup
import os

# .................................................................................
def get_anthem_url(country_url:str)->str:
    print(country_url, '\n')
    response = requests.get(country_url)
    if response.status_code != 200:
        return(None)
    else:
        soup = BeautifulSoup(response.text, "html.parser")
        infobox = soup.find("table", attrs={'class': 'infobox'}) 
        if infobox is not None:
            mediaContainer = infobox.find("div", attrs={'class': 'mediaContainer'})
            if mediaContainer is not None:
                # print(mediaContainer)
                # беру первую попавшуюся ссылку на аудио-файл
                source = mediaContainer.find('source')
                if source is not None:
                    if source.has_attr('src'):
                        return ('http:' + source['src'])
    return(None)

# ---------------------------------------------------------------------------------
# загрузка файла
def download_file(url: str, filename: str):
    # print(f'save: {filename}')
    with open(os.path.join('data', filename), mode='wb') as f:
        response = requests.get(url)
        f.write(response.content)
        return(response.status_code)
    return None
