"""
Микросервис для загрузки гимна и отправки его на почту

Через RabbitMQ получает и обрабатывает запрос:
{'email': 'Адрес почты получателя', 
 'country': 'Название_страны', 
 'url': 'Ссылка на веб-сайт данной страны'
}

Настройки:
переменные окружения
RMQ_(HOST/PORT/USER/PASSWORD) - параметры соединения с сервисом RMQ
RMQ_EXCH_ANTHEM - канал для получения запросов
"""

from butorin_sendmail import send_email
import pika
import os
import json

from load_anthem import download_file, get_anthem_url

# import sentry_sdk

# sentry_sdk.init(
#     dsn="https://23e852b1b1a24965bd26a9b73459b255@o1095196.ingest.sentry.io/6130526",

#     # Set traces_sample_rate to 1.0 to capture 100%
#     # of transactions for performance monitoring.
#     # We recommend adjusting this value in production.
#     traces_sample_rate=1.0
# )

# ---------------------------------------------------------------------------------
def cb(ch, method, properties, body):
    """Коллбек-процедура для получения сообщения из очереди RMQ
    """
    try:
        in_params = json.loads(body)
        load_n_send_anthem(in_params['country'], in_params['url'], in_params['email'])
    except Exception as ex:
        # pass
        raise ValueError(f'exception in call-back: [{ex.args[0]}]')
        # любые ошибки просто игнорирую (ошибки во входных данных, при обработке и т.п.)

# ---------------------------------------------------------------------------------
def load_n_send_anthem(country: str, url: str, email: str):
    anthem_url = get_anthem_url(url)
    file_anthem = None

    if anthem_url is None:
        msg_body = f"""Не удалось найти ссылку на гимн для страны.\n\n
Страна: {country}\n
Веб-страница на вики: {url}"""
    else:
        # скачиваем гимн
        dir_temp = os.path.join(os.getcwd(), 'temp')
        if not os.path.isdir(dir_temp):
            os.mkdir(dir_temp)
        file_anthem = os.path.join(dir_temp, f'{anthem_url.split("/")[-1]}')
        result_save = download_file(anthem_url, file_anthem)
        if result_save != 200:
            msg_body = f"""Ошибка при загрузке гимна страны (код {result_save}).\n\n
Страна: {country}\n
Веб-страница на вики: {url}\n
Ссылка на гимн: {anthem_url}"""
            file_anthem = None
        else:
            msg_body = f"""Гимн запрошенной страны во вложении.\n\n
Страна: {country}\n
Веб-страница на вики: {url}\n
Ссылка на гимн: {anthem_url}"""
    # отправка результата работы на почту
    attaches = []
    if file_anthem is not None:
        attaches.append(file_anthem)
    send_email(email_to=email, subj=f'anthem of {country}', body=msg_body, attaches=attaches)

# ---------------------------------------------------------------------------------

# считываем настройки
user = os.environ.get("RMQ_USER")
password = os.environ.get("RMQ_PASSWORD")
host = os.environ.get("RMQ_HOST")
port = os.environ.get("RMQ_PORT")
exch_name = os.environ.get("RMQ_EXCH_ANTHEM")

# коннект к RMQ, запуск потребителя
print(f'connecting to RMQ: [{host}:{port}]')

parameters = pika.URLParameters(f"amqp://{user}:{password}@{host}:{port}")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.exchange_declare(exchange=exch_name, exchange_type="fanout")

result = channel.queue_declare(queue="", exclusive=False)
queue_name = result.method.queue
channel.queue_bind(exchange=exch_name, queue=queue_name)

channel.basic_consume(queue=queue_name, on_message_callback=cb, auto_ack=True)

print("waiting for requests to download anthem")
channel.start_consuming()
