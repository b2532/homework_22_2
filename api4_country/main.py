"""
Микросервис для поиска веб-страницы заданной страны на WIKI

Через RabbitMQ получает и обрабатывает запрос:
{'email': 'Адрес почты получателя', 
 'country': 'Название_страны'
}

Найденный адрес веб-страницы сохраняет в кэш и передаёт в сервис скачивания гимна

Настройки:
переменные окружения
RMQ_(HOST/PORT/USER/PASSWORD) - параметры соединения с сервисом RMQ
RMQ_EXCH_COUNTRY_SITE - канал для получения запросов
"""

import pika
import os
import json
import requests

from find_country import get_country_WIKI_site
from rmq_anthem import send_RMQ_get_anthem

# import sentry_sdk

# sentry_sdk.init(
#     dsn="https://23e852b1b1a24965bd26a9b73459b255@o1095196.ingest.sentry.io/6130526",

#     # Set traces_sample_rate to 1.0 to capture 100%
#     # of transactions for performance monitoring.
#     # We recommend adjusting this value in production.
#     traces_sample_rate=1.0
# )

# ---------------------------------------------------------------------------------
def cb(ch, method, properties, body):
    """Коллбек-процедура для получения сообщения из очереди RMQ
    """
    try:
        in_params = json.loads(body)
        # ищем веб-сайт страны
        country_find=get_country_WIKI_site(in_params['country'])
        # сохраняем в кеш (если поиск успешный)
        if country_find['result'] == '1':
            save_cache(in_params['country'], country_find['url'])
        # и передаём его в api-5
        send_RMQ_get_anthem(in_params['country'], country_find['url'], in_params['email'])
    except Exception as ex:
        # pass
        raise ValueError(f'exception in call-back: [{ex.args[0]}]')
        # любые ошибки просто игнорирую (ошибки во входных данных, при обработке и т.п.)

# ---------------------------------------------------------------------------------
def save_cache(country: str, url: str):
    host_cache = os.environ.get("API_CACHE")
    params = {'country': country, 'url': url}
    resp = requests.post(host_cache+"/save", data=json.dumps(params))
    print(resp.text)

# ---------------------------------------------------------------------------------
# считываем настройки
user = os.environ.get("RMQ_USER")
password = os.environ.get("RMQ_PASSWORD")
host = os.environ.get("RMQ_HOST")
port = os.environ.get("RMQ_PORT")
exch_name = os.environ.get("RMQ_EXCH_COUNTRY_SITE")

# коннект к RMQ, запуск потребителя
print(f'connecting to RMQ: [{host}:{port}/exch:{exch_name}]')

parameters = pika.URLParameters(f"amqp://{user}:{password}@{host}:{port}")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.exchange_declare(exchange=exch_name, exchange_type="fanout")

result = channel.queue_declare(queue="", exclusive=False)
queue_name = result.method.queue
channel.queue_bind(exchange=exch_name, queue=queue_name)

channel.basic_consume(queue=queue_name, on_message_callback=cb, auto_ack=True)

print("waiting for requests to search country web-site")
channel.start_consuming()
