import requests
from bs4 import BeautifulSoup

BASE_URL = 'https://en.wikipedia.org'
URL_LIST_COUNTRIES='https://en.wikipedia.org/wiki/List_of_countries_by_population_(United_Nations)'

# .................................................................................
def get_country_WIKI_site(country: str) -> dict:
    res = {'result':'0', 'url':f'country not found - [{country}]'}
    response = requests.get(URL_LIST_COUNTRIES)
    if response.status_code != 200:
        res['url']=f'List of coutries is unavailable ({URL_LIST_COUNTRIES})'
    else:
        soup = BeautifulSoup(response.text, "html.parser")
        table = soup.find("table") 
        if table is not None:
            for cell in table.find_all("td")[::6]:
                try:
                    country_name = cell.find('span', attrs={'class': 'datasortkey'}).text.strip()
                except:
                    country_name = ''
                if country_name.upper() == country.upper():
                    # print(f'country_name={country_name}')
                    href=cell.find("a")
                    # print(href['href'])
                    if href is not None:
                        res['result']='1'
                        res['url']=BASE_URL+href['href']
    return(res) 
