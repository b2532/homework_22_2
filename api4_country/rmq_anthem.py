import pika
import json
import os

def send_RMQ_get_anthem(country, country_url, email):
    # считываем настройки
    user = os.environ.get("RMQ_USER")
    password = os.environ.get("RMQ_PASSWORD")
    host = os.environ.get("RMQ_HOST")
    port = os.environ.get("RMQ_PORT")
    exch_name = os.environ.get("RMQ_EXCH_ANTHEM")

    # коннект к RMQ, запуск потребителя
    print(f'connecting to RMQ: [{host}:{port}/exch:{exch_name}]')

    parameters = pika.URLParameters(f"amqp://{user}:{password}@{host}:{port}")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.exchange_declare(exchange=exch_name, exchange_type="fanout")

    result = channel.queue_declare(queue="", exclusive=False)
    queue_name = result.method.queue
    channel.queue_bind(exchange=exch_name, queue=queue_name)

    params = {'email': email, 'country': country, 'url': country_url}
    channel.basic_publish(exchange=exch_name, routing_key="", body=f"{json.dumps(params)}")
