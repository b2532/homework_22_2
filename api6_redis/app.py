"""
Микросервис для хранения кешированных данных
--------------------------------------------
/find: GET-запрос на поиск значения в кэше
/save: POST-запрос на сохранения значения в кэше

--
Настройки:
переменные окружения
REDIS_(HOST/PORT) - параметры соединения с сервисом Redis
"""

import redis
import os
import json
import logging

from flask import Flask, request, jsonify

app = Flask(__name__)

redis_host = os.environ.get("REDIS_HOST")
redis_port = os.environ.get("REDIS_PORT")

cache = redis.Redis(host=redis_host, port=redis_port)

debug_logger = logging.getLogger("debug_logger")
debug_logger.setLevel(logging.DEBUG)
fh = logging.FileHandler("api6.log" ,"w", "utf-8")
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
fh.setFormatter(formatter)
debug_logger.addHandler(fh)

# ------------------------------------------------------------------------------------
@app.route("/")
def home():
    return f"<h2>mbutorin cache service (Redis)</h2>"

# ------------------------------------------------------------------------------------
@app.route("/find", methods=["GET"])
def search_in_cache():
    """Поиск значения в кеше
    • GET-запрос (/find)
    • параметры запроса (json): {"country": "Название страны"}

    Returns:
        json: 
            {'result': 0 (если данные не найдены) или 1 (если данные в кэше найдены), 
            'url': Кешированное значение}        
    """

    answer = {'result': 0}
    try:
        debug_logger.debug(f'/find: input data={request.data}')
        # data = request.get_json()
        data = json.loads(request.data.decode('utf-8'))
        debug_logger.debug(f'/find: input json={data}')

        cache_answer = cache.get(data['country'].upper())
        if cache_answer is not None:
            answer = {'result': 1, 'url': cache_answer.decode('utf-8')}
    except Exception as ex:
        answer['exception'] = ex.args[0]
    debug_logger.debug(f'/find: output ans={answer}')
    return jsonify(answer)

# ------------------------------------------------------------------------------------
@app.route("/save", methods=["POST"])
def save_in_cache():
    """Сохранение значения в кеше
    • POST-запрос (/save)
    • параметры запроса (json): {"country": "Название страны", "url": "веб-страница страны на wiki"}

    Returns:
        str: текстовое описание результата
    """

    # data = request.json
    data = json.loads(request.data.decode('utf-8'))
    debug_logger.debug(f'/save: input data={request.data}')

    try:
        key = data['country'].upper()
        cache.mset({key: data['url']})
        debug_logger.debug(f'/save OK, key={key} value={data["url"]}')
    except Exception as ex1:
        debug_logger.debug(f'/save ERROR: {ex1.args[0]}')
        return f"ERROR while saving: {ex1.args[0]}"
    return "OK, value saved in cache"

if __name__ == "__main__":
    app.run()
